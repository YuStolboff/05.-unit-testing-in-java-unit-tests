package app;

import java.math.BigInteger;

import static java.math.BigInteger.valueOf;

public class Utils {

    public static String concatenateWords(String firstWord, String secondWord) {
        return firstWord + secondWord;
    }

    public static BigInteger computeFactorial(int number) {
        if (number < 0) {
            throw new StackOverflowError();
        }
        if (number <= 1) {
            return valueOf(1);
        }
        return computeFactorial(number - 1).multiply(valueOf(number));
    }
}
