package util;

import java.math.BigInteger;

public class Assert {

    public static void assertEquals(String testName, String expected, String actual) {
//        if (expected == null && actual == null) {
//            System.out.println(testName + " passed");
//        } else if (expected != null && actual != null) {
            if (expected.equals(actual)) {
                System.out.println(testName + " passed");
            } else {
                System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
            }
        //}
    }

    public static void assertEquals(String testName, BigInteger expected, BigInteger actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }
}
