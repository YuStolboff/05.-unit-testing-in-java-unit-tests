import org.junit.Ignore;
import org.junit.Test;
import util.RandomGenerator;

import static app.Utils.computeFactorial;
import static app.Utils.concatenateWords;
import static java.math.BigInteger.ONE;
import static java.math.BigInteger.valueOf;
import static org.junit.Assert.assertEquals;

public class UtilsTest {

    @Test
    public void testConcatenateWordsSimple() {
        assertEquals("TestConcatenateWords simple test failed", "qwerty", concatenateWords("qwe", "rty"));
    }

    @Test
    public void testConcatenateWordsEmpty() {
        assertEquals("TestConcatenateWords empty test failed", "", concatenateWords("", ""));
    }

    @Test
    public void testConcatenateWordsNullAllParam() {
        assertEquals("TestConcatenateWords null all param test failed", "nullnull", concatenateWords(null, null));
    }

    @Test
    public void testConcatenateWordsNullFirstParam() {
        assertEquals("TestConcatenateWords null first param test failed", "qwenull", concatenateWords("qwe", null));
    }

    @Test
    public void testConcatenateWordsNullSecondParam() {
        assertEquals("TestConcatenateWords null second param test failed", "nullrty", concatenateWords(null, "rty"));
    }

    @Test
    public void testConcatenateWordsNonLatin() {
        assertEquals("TestConcatenateWords non-latin test failed", "qweфыв", concatenateWords("qwe", "фыв"));
    }

    @Test
    @Ignore
    public void testComputeFactorialSimple() {
        assertEquals("TestComputeFactorial simple test failed", valueOf(39916800), computeFactorial(11));
    }

    @Test
    @Ignore
    public void testComputeFactorialZero() {
        assertEquals("TestComputeFactorial 1 test failed", ONE, computeFactorial(0));
    }

    @Test
    @Ignore
    public void testComputeFactorialOne() {
        assertEquals("TestComputeFactorial 1 test failed", ONE, computeFactorial(1));
    }

    @Test(expected = StackOverflowError.class)
    @Ignore
    public void testComputeFactorialForNegativeNumbers() {
        computeFactorial(-10);
    }

    private RandomGenerator randomGenerator = new RandomGenerator();

    @Test(timeout = 200)
    public void testFactorialWithTimeout() {
        int factorialParam = randomGenerator.generatesRandomForFactorial();
        computeFactorial(factorialParam);
    }
}
